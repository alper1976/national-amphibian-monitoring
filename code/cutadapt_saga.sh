
######################################
### Amphibian and Fish eDNA

### cutadapt
module load cutadapt/1.18-foss-2018b-Python-3.6.6

## Amphibians

RAWDIR=/cluster/projects/nn9744k/01_raw_data
RESDIR=/cluster/projects/nn9744k/02_results

# transfer files to scratch

mkdir /cluster/projects/nn9744k/02_results/09_fishphibia/AdaptersRemoved
mkdir /cluster/projects/nn9744k/02_results/14_amphibia/AdaptersRemoved



##############################################
### run cutadapt to remove primer sequences
##############################################

cd $RAWDIR/09_fishphibia

for f in *.fastq.gz
    do
        FILE=${f#$DIRS}
        if [[ $FILE =~ R1 ]]
        then
            echo $FILE
            echo ${FILE//R1/R2}
        fi
    done

for f in *.fastq.gz
    do
        FILE=${f#$DIRS}
        if [[ $FILE =~ R1 ]] #If file name contains R1
        then
            cutadapt -g ACACCGCCCGTCACCCT...AAGTCGTAACATGGTAAGTRTAC -G GTAYACTTACCATGTTACGACTT...AGGGTGACGGGCGGTGT -o $RESDIR/09_fishphibia/AdaptersRemoved/$FILE -p $RESDIR/09_fishphibia/AdaptersRemoved/${FILE//R1/R2} --discard-untrimmed --minimum-length 35 $FILE ${FILE//R1/R2}

        fi
    done

cd $RAWDIR/14_amphibia
for f in *.fastq.gz
    do
        FILE=${f#$DIRS}
        if [[ $FILE =~ R1 ]] #If file name contains R1
        then
            cutadapt -g ACACCGCCCGTCACCCT...AAGTCGTAACATGGTAAGTRTAC -G GTAYACTTACCATGTTACGACTT...AGGGTGACGGGCGGTGT -o $RESDIR/14_amphibia/AdaptersRemoved/$FILE -p $RESDIR/14_amphibia/AdaptersRemoved/${FILE//R1/R2} --discard-untrimmed --minimum-length 35 $FILE ${FILE//R1/R2}

        fi
    done
