# National amphibian monitoring

Freshwaters represent the most threatened environments with regard to biodiversity loss and therefore there is a need for national monitoring programs to effectively document species distribution and evaluate potential risks for vulnerable species. The monitoring of species for designing effective management practices is however challenged by insufficient data acquisition when using traditional methods. Here we provide the code for a paper on the application of environmental DNA (eDNA) metabarcoding of amphibians in combination with quantitative PCR assays for an invasive pathogenic chytrid species (Batrachochytrium dendrobatidis -Bd), a potential threat to endemic and endangered amphibian species. 

## Description
This repository includes code and some data with raw data for download at SRA under accession XXXXX. It includes a database file - Swedish_amphibians_ref_db.fasta - the metadata from all samples - final_metafile.csv - and the final species observations - amp_obs.csv.
In the subfolders 2019 and 2020 you can find the outputs of the dada2 analysis including species identification.

The code includes bash scripts to run cutadapt on the amphibian data - cutadapt_saga.sh - the dada2 R scripts and the slurm script to run the dada2 pipeling - dada2_amphibians2019_saga.R; dada2_amphibians2020_saga.R; run_dada2.slurm.
In addition, R scripts to perform data visualization and statistics are provided for QPCR results - bd.R - and amphibian metabarcoding - seq_data.R

## Support
This project is not funded anymore thus support is highly restricted.

## Authors and acknowledgment
Alexander Eiler, Mats Töpel, Omneya Ahmed and Tomas Larsson contributed to the project.

## License
Lesser General Public License (LGPL).

## Project status
Development of this project is currently on hold.
